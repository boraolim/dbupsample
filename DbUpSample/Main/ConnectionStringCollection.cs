﻿using System;

namespace DbUpSample
{
  [Serializable]
  public class ConnectionStringCollection
  {
    public string ConnectionStringSQLServer { get; set; }
    public string ConnectionStringMySQLServer { get; set; }
  }
}
