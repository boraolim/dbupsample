﻿-- Migration_20210612.sql

-- Autor: Olimpo Bonilla Ramírez.
-- Objetivo: Creación de las tablas de Base de Datos.
-- Fecha: 2021-06-12.
-- Comentarios: Ejecutar aquí para cuestiones de pruebas todo lo que se ha implementado en la Base de Datos.

exec sp_refreshview @viewname =  'dbo.vw_articles';

-- Migration_20210612.sql