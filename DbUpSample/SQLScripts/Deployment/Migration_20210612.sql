﻿-- Migration_20210612.sql

-- Autor: Olimpo Bonilla Ramírez.
-- Objetivo: Creación de las tablas de Base de Datos.
-- Fecha: 2021-06-12.
-- Comentarios: Aquí se crean los objetos de Base de Datos (Tablas, vistas, SP, etc) asignados a un esquema de Base de Datos o bien asignados al esquema por default de Base de Datos.

-- 1. Catálogo de tiendas.
--------------------------
IF OBJECT_ID('dbo.mtStores') IS NOT NULL
BEGIN
  DROP TABLE dbo.mtArticles; DROP TABLE dbo.mtStores;

  IF OBJECT_ID('dbo.mtStores') IS NOT NULL
    PRINT '<<< Ocurrió un error al eliminar el objeto ''dbo.mtStores''. >>>';
  ELSE
    PRINT '<<< El objeto ''dbo.mtStores'' se ha eliminado correctamente. >>>';
END;

CREATE TABLE dbo.mtStores
(
  [store_id]       [int]                        IDENTITY(1, 1) NOT NULL,
  [name]           [varchar]       (255)        NOT NULL,
  [address]        [varchar]       (255)        NOT NULL DEFAULT 0,
  CONSTRAINT       [pk_IdStore]                 PRIMARY KEY (store_id),
  CONSTRAINT       [uq_IdStore]                 UNIQUE(store_id)
);
GO

IF OBJECT_ID('dbo.mtStores') IS NOT NULL
  PRINT '<<< El objeto ''dbo.mtStores'' se ha creado correctamente. >>>';
ELSE
  PRINT '<<< Ocurrió un error al crear el objeto ''dbo.mtStores''. >>>';

-- 2. Catálogo de artículos.
----------------------------
IF OBJECT_ID('dbo.mtArticles') IS NOT NULL
BEGIN
  DROP TABLE dbo.mtArticles;

  IF OBJECT_ID('dbo.mtArticles') IS NOT NULL
    PRINT '<<< Ocurrió un error al eliminar el objeto ''dbo.mtArticles''. >>>';
  ELSE
    PRINT '<<< El objeto ''dbo.mtArticles'' se ha eliminado correctamente. >>>';
END;

CREATE TABLE dbo.mtArticles
(
  [id_article]     [int]                        IDENTITY(1, 1) NOT NULL,
  [name]           [varchar]       (255)        NOT NULL,
  [description]    [varchar]       (255)        NOT NULL DEFAULT 0,
  [price]          [money]                      NOT NULL,
  [total_in_shelf] [int]                        NOT NULL DEFAULT 0,
  [total_in_vault] [int]                        NOT NULL DEFAULT 0,
  [store_id]       [int]                        NOT NULL,
  CONSTRAINT       [pk_Idarticle]               PRIMARY KEY (id_article, store_id),
  CONSTRAINT       [uq_Idarticle]               UNIQUE (id_article, store_id),
  CONSTRAINT       [fk_Idarticle]               FOREIGN KEY(store_id) REFERENCES mtStores(store_id)
);
GO

IF OBJECT_ID('dbo.mtArticles') IS NOT NULL
  PRINT '<<< El objeto ''dbo.mtArticles'' se ha creado correctamente. >>>';
ELSE
  PRINT '<<< Ocurrió un error al crear el objeto ''dbo.mtArticles''. >>>';

-- 3. Inventario de artículos.
------------------------------
IF OBJECT_ID('dbo.vw_articles') IS NOT NULL
BEGIN
  DROP VIEW dbo.vw_articles;

  IF OBJECT_ID('dbo.vw_articles') IS NOT NULL
    PRINT '<<< Ocurrió un error al eliminar el objeto ''dbo.vw_articles''. >>>';
  ELSE
    PRINT '<<< El objeto ''dbo.vw_articles'' se ha eliminado correctamente. >>>';
END;
GO

CREATE VIEW dbo.vw_articles AS
  SELECT t1.id_article, t1.description, t1.name,
         t1.price, t1.total_in_shelf, t1.total_in_vault,
		  	 t2.name [store_name]
    FROM dbo.mtArticles t1
   INNER JOIN dbo.mtStores t2 ON (t2.store_id = t1.store_id);
GO

IF OBJECT_ID('dbo.vw_articles') IS NOT NULL
  PRINT '<<< El objeto ''dbo.vw_articles'' se ha creado correctamente. >>>';
ELSE
  PRINT '<<< Ocurrió un error al crear el objeto ''dbo.vw_articles''. >>>';

-- 4. Store Procedure de artículos por sucursal.
------------------------------------------------
IF OBJECT_ID('dbo.procArticlesGet') IS NOT NULL
BEGIN
  DROP PROCEDURE dbo.procArticlesGet;

  IF OBJECT_ID('dbo.procArticlesGet') IS NOT NULL
    PRINT '<<< Ocurrió un error al eliminar el objeto ''dbo.procArticlesGet''. >>>';
  ELSE
    PRINT '<<< El objeto ''dbo.procArticlesGet'' se ha eliminado correctamente. >>>';
END;
GO

CREATE PROCEDURE dbo.procArticlesGet (@StoreId INT) AS
  SELECT t1.id_article, t1.description, t1.name,
         t1.price, t1.total_in_shelf, t1.total_in_vault,
		  	 t2.name [store_name]
    FROM dbo.mtArticles t1
   INNER JOIN dbo.mtStores t2 ON (t2.store_id = t1.store_id)
   WHERE (t1.store_id = @StoreId);
GO

IF OBJECT_ID('dbo.procArticlesGet') IS NOT NULL
  PRINT '<<< El objeto ''dbo.procArticlesGet'' se ha creado correctamente. >>>';
ELSE
  PRINT '<<< Ocurrió un error al crear el objeto ''dbo.procArticlesGet''. >>>';

-- Migration_20210612.sql
